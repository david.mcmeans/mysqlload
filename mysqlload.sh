#!/bin/bash

# $0 is the script name, $1 is the first argument, $2 is the second...
# VAR=${1:-DEFAULTVALUE}

TABLE="$1"
INPUTFILE="$2"
SKIPROWS=${3:-1}

# default for CSV
LINETERM=${4:-"\r\n"}
FIELDTERM=${5:-","}
ENCLOSEDBY=${6:-"\""}

# CRED=${7:-"\"\""}
# _DBUSER="$(cut -d'\"' -f1 <<<'$CRED')"
# _DBPASS="$(cut -d'\"' -f2 <<<'$CRED')"
# _DBNAME="$(cut -d'\"' -f3 <<<'$CRED')"

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

WORKDIR="$DIR/import"

# get db credentials
source $DIR/conf/db.settings

MYSQL="/usr/bin/mysql"

# clear table prior to load
$MYSQL -h localhost -u $DBUSER -p$DBPASS $DBNAME --execute="delete from $TABLE"

# build execute statement
EXECUTE="LOAD DATA LOCAL INFILE '$INPUTFILE' INTO TABLE $TABLE "
EXECUTE+="FIELDS TERMINATED BY '$FIELDTERM' OPTIONALLY ENCLOSED BY '$ENCLOSEDBY' "
EXECUTE+="LINES TERMINATED BY '$LINETERM' "
EXECUTE+="IGNORE $SKIPROWS LINES; SHOW WARNINGS "

$MYSQL --local-infile -h localhost -u $DBUSER -p$DBPASS $DBNAME --execute="$EXECUTE" > $WORKDIR/$TABLE.log

# num warnings in log file
NUMLINES=$(< "$WORKDIR/$TABLE.log" wc -l)

exit $NUMLINES
