# mysqlload

MySQL bash load script suitable for calling from Drupal Rules

Requires an environment in which you are permitted to call mysql from the command line. Shared hosting
usually does not permit this.

## Configuration

Copy conf/example.db.settings to conf/db.settings and specify the connection details in db.settings.

## Calling

**mysqlload.sh tablename inputfile [skiprows] [lineterm] [fieldterm] [enclosedby]**	

where **skiprows** is the number of initial rows to skip, **lineterm** is the line terminator, 
**fieldterm** is the field terminator and **enclosedby** is the character optionally enclosing fields.

Ex.

<pre>mysqlload.sh employees /tmp/employees.csv 1 \r\n , "</pre>

#### Defaults

The defaults expect a comma-separated value (CSV) file with *\r\n* line terminators and a one-row header. 
If this is your case, the last four parameters could be skipped. 

### Calling From a Drupal Rule

In a Drupal Rule action for "Execute a shell command", with **Command** value set to */bin/bash*,
the **Arguments** value might look like

<pre>
/home/bob/scripts/mysqlload.sh
employees
/home/bob/www/sites/mysite.net/files/private/export/employees.csv
1
\r\n
,
"
</pre>

If you have a simple CSV file with a one row header, your action argument value could be simply

<pre>
/home/bob/scripts/mysqlload.sh
employees
/home/bob/www/sites/mysite.net/files/private/export/employees.csv
</pre>





